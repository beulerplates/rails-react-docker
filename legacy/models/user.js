import { DataTypes } from "sequelize";

export default (sequelize) => {
  const User = sequelize.define(
    "User",
    {
      username: DataTypes.STRING,
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      thumbs: DataTypes.INTEGER,
      wins: DataTypes.INTEGER,
    },
    {
      tableName: "users",
    }
  );
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};
