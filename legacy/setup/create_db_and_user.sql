create database one_hour_beats;
create user ohb with encrypted password 'ohb';
grant all privileges on database one_hour_beats to ohb;