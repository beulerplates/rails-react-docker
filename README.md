# Beulerplates - Rails-React-Postgres! (in Docker)

- runs in a docker-compose pod
- can easily deploy to Kube or whatever
- has Action Cable boilerplate for real time brouhaha

## Instructions

### First build everything required

```
docker-compose build
```

### Generate your rails scaffolds

```
docker
```

### Finalize building, create and seed database

```sh
docker-compose up
docker-compose run --rm frontend npm i
docker-compose run --rm backend bash
> rails db:create
> rails db:migrate
> rails db:seed
```

### Run dis bitch!

```sh
docker-compose up [-d] # the -d is for daemon mode
```

### Handy commands

```sh
docker-compose down && docker-compose up # reset the docker environment, useful for after chaning the docker-compose or what have you.
docker-compose run --rm [service] [?command] # run an instance of a service with optional command
```
