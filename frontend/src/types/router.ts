export type RoutedProps = {
  history: History
  match: { params: { id: string } }
  location: Location
}
