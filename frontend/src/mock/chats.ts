import { Chat } from '../types/view'

export const chatIndex: Record<string, Chat[]> = {
  'dank-rhinos': [
    { userId: 'eli7vh', message: 'hello world' },
    {
      userId: 'metacusis',
      message:
        'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.',
    },
    {
      userId: 'chocobobafett',
      message:
        'I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.',
    },
    {
      userId: 'eli7vh',
      message:
        'When I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms',
    },
    {
      userId: 'eli7vh',
      message:
        'When I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms',
    },
  ],
}
